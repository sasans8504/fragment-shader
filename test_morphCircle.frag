#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform float u_time;
uniform vec2 u_mouse;

vec3 get_circle(vec2 position, vec3 color, float size){
    float circle = sqrt(pow(position.x, 2.0) + pow(position.y, 2.0));
    circle = smoothstep(size, size + 0.003, 1.0 - circle);

    return color * circle;
}

float random2d(vec2 coord){
    return fract(sin(dot(coord.xy, vec2(12.9898, 78.233))) * 43758.5453);
}

float transparent(vec3 canvas){
    return canvas.r + canvas.g + canvas.b;
}

void main(){
    vec2 coord_m = gl_FragCoord.xy * 0.12;
    coord_m -= u_time + vec2(sin(coord_m.y), cos(coord_m.x));

    float ran01 = fract(random2d(floor(coord_m)) + u_time / 60.0);
    float ran02 = fract(random2d(floor(coord_m)) + u_time / 40.0);

    ran01 *= 0.3 - length(fract(coord_m));

    vec2 coord = gl_FragCoord.xy / u_resolution;
    vec3 circle_color = vec3(0.0, 0.0, 0.0);
    circle_color.r += ran01 * 3.0;
    circle_color.g += ran02 * ran01 * 3.0;
    circle_color.b += ran02 * ran01 * 7.0;
    vec3 canvas = vec3(0.0);

    vec3 circle = get_circle(coord - vec2(0.5), circle_color, 0.7);
    canvas += circle;

    gl_FragColor = vec4(canvas, ceil(canvas));
}