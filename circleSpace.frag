#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 u_mouse;

float xor(float a, float b){
    return a * (1.-b) + b*(1.-a);
}

float transparent(vec3 canvas){
    return canvas.r + canvas.g + canvas.b;
}

void main(){
    vec2 coord = (gl_FragCoord.xy - (0.5 * u_resolution.xy)) / u_resolution.y;
    vec3 color = vec3(0.2392 * sin(u_time), 0.2235 * sin(u_time), 0.0588 * cos(u_time));

    float angle = .78;
    float s = sin(angle);
    float c = cos(angle);
    coord *= mat2(c, -s, s, c);
    coord *= 15.;

    vec2 gv = fract(coord) - .5;
    vec2 id = floor(coord);

    float m = 0.0;
    float t = u_time;
    
    for(float y = -1.; y <= 1.; y++){
        for(float x = -1.; x <= 1.; x++){
            vec2 offset = vec2(x, y);

            float d = length(gv-offset);
            float dist = length(id+offset)*.3;

            float r = mix(.3, 1.5, sin(dist-t) * .5+.5);
            m = xor(m, smoothstep(r, r * .9, d));
        }
    }

    //color.rg = gv;
    // color += mod(m, 2.);
    color += m;

    gl_FragColor = vec4(color, 1.0);
}