#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;

void main(){
    vec2 coord = (gl_FragCoord.xy / u_resolution);
    float color = 1.0;

    float size = 60.0;

    float alpha = sin(floor(coord.x * size) + u_time * 4.0 + 1.0 / 2.0);

    gl_FragColor = vec4(vec3(color * coord.x, color * coord.y, color), alpha);
}