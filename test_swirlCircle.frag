#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 u_mouse;

vec3 get_circle(vec2 position, vec3 color, float size){
    float circle = sqrt(pow(position.x, 2.0) + pow(position.y, 2.0));
    circle = smoothstep(size, size + 0.003, 1.0 - circle);

    return color * circle;
}

float transparent(vec3 canvas){
    return canvas.r + canvas.g + canvas.b;
}

void main(){
    vec2 coord = gl_FragCoord.xy / u_resolution;
    vec3 color = vec3(0.0, 0.0, 0.0);
    coord += -u_mouse / u_resolution;

    coord.x += 0.5;
    coord.y += 0.25;
    float angle = atan(-coord.y + 0.25, coord.x - 0.5) * 0.1;
    float len = length(coord - vec2(0.5, 0.25));
    coord.x -= 0.5;
    coord.y -= 0.25;
    
    color.r += sin(len * 40.0 + angle * 40.0 - u_time) * 1.6;
    color.g += cos(len * 30.0 + angle * 60.0 + u_time) * 2.8;
    color.b += sin(len * 50.0 + angle * 80.0 - sin(u_time)) * 2.6;
    vec3 canvas = vec3(0.0);
    // coord += -u_mouse / u_resolution;

    vec3 circle = get_circle(coord, color, 0.6);
    canvas += circle;

    gl_FragColor = vec4(canvas, transparent(canvas));
}