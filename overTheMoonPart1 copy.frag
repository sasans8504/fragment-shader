#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
#define S(a, b, t) smoothstep(a, b, t)

float taperBox(vec2 p, float wb, float wt, float yb, float yt, float blur){
    float m = S(-blur, blur, p.y-yb);
    m *= S(blur, -blur, p.y-yt);
    p.x = abs(p.x);

    // 0 p.y = yb, 1 p.y = yt
    float w = mix(wb, wt, (p.y - yb) / (yt - yb));
    m *= S(blur, -blur, p.x-w);
    
    return m;
}

vec4 drawTree(vec2 uv, vec3 color, float blur){
    float m = taperBox(uv, .03, .03, -.05, .25, blur); //trunk
    m += taperBox(uv, .2, .1, .25, .5, blur); //canopy 1
    m += taperBox(uv, .15, .05, .5, .75, blur); //canopy 2
    m += taperBox(uv, .1, .0, .75, 1., blur); //top

    float shadow = taperBox(uv-vec2(.2, 0), .1, .5, .15, .25, blur);
    shadow += taperBox(uv+vec2(.25, 0), .1, .5, .45, .5, blur);
    shadow += taperBox(uv-vec2(.25, 0), .1, .5, .7, .75, blur);
    color -= shadow*.8;
    //m = 1.;
    return vec4(color, m);
}

float getHeight(float x){
    return sin(x*.423)+sin(x)*.3;
}

void main(){
    vec2 uv = (gl_FragCoord.xy-.5*u_resolution.xy)/u_resolution.y;
    uv.x += u_time*.05;
    uv *= 8.;
    vec4 color = vec4(0.0);

    float blur = .005;
    
    float id = floor(uv.x);
    float n = fract(sin(id*234.12)*5463.3)*2.-1.;
    float x = n*.3;
    float y = getHeight(uv.x);
    color += S(blur, -blur, uv.y+y); //ground

    y = getHeight(id+.5+x);
    //bunch of trees for only draw 1 tree
    uv.x = fract(uv.x)-.5;
    
    vec4 tree = drawTree((uv-vec2(x, -y))*vec2(1., 1.+n*.2), vec3(1.), blur);

    //color.rg = uv;

    color = mix(color, tree, tree.a);

    

    float thickness = 1./u_resolution.y;
    // if(abs(uv.x)<thickness) color.g = 1.;
    // if(abs(uv.y)<thickness) color.r = 1.;
    

    gl_FragColor = color;
}