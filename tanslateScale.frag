#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform float u_time;

mat2 scale(vec2 scale){
    return mat2(scale.x, 0.0, 0.0, scale.y);
}

float circleShape(vec2 position, float radius){
    return step(radius, length(position - vec2(0.5)));
}

void main(){
    vec2 coord = gl_FragCoord.xy / u_resolution;
    vec3 color = vec3(0.6471, 0.3373, 0.7255);

    vec2 translate = vec2(0.3, 0.0);
    coord += translate * -0.5;

    coord -= vec2(0.5);
    coord = scale(vec2(sin(u_time) + 1.5)) * coord;
    coord += vec2(0.5);

    color += vec3(circleShape(coord, 0.3));

    gl_FragColor = vec4(color, 1.0);
}