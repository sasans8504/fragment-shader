#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
#define S(a, b, t) smoothstep(a, b, t)

float taperBox(vec2 p, float wb, float wt, float yb, float yt, float blur){
    float m = S(-blur, blur, p.y-yb);
    m *= S(blur, -blur, p.y-yt);
    p.x = abs(p.x);

    // 0 p.y = yb, 1 p.y = yt
    float w = mix(wb, wt, (p.y - yb) / (yt - yb));
    m *= S(blur, -blur, p.x-w);
    
    return m;
}

vec4 treeDraw(vec2 uv, vec3 color, float blur){
    float m = taperBox(uv, .03, .03, -.05, .25, blur); //trunk
    m += taperBox(uv, .2, .1, .25, .5, blur); //canopy 1
    m += taperBox(uv, .15, .05, .5, .75, blur); //canopy 2
    m += taperBox(uv, .1, .0, .75, 1., blur); //top

    float shadow = taperBox(uv-vec2(.2, 0), .1, .5, .15, .25, blur);
    shadow += taperBox(uv+vec2(.25, 0), .1, .5, .45, .5, blur);
    shadow += taperBox(uv-vec2(.25, 0), .1, .5, .7, .75, blur);
    color -= shadow*.8;
    //m = 1.;
    return vec4(color, m);
}

float getHeight(float x){
    return sin(x*.423)+sin(x)*.3;
}

vec4 layerDraw(vec2 uv, float blur){
    vec4 color = vec4(0.0);
    float id = floor(uv.x);
    float n = fract(sin(id*234.12)*5463.3)*2.-1.;
    float x = n*.3;
    float y = getHeight(uv.x);
    float ground = S(blur, -blur, uv.y+y); //ground
    color += ground;

    y = getHeight(id+.5+x);
    //bunch of trees for only draw 1 tree
    uv.x = fract(uv.x)-.5;
    
    vec4 tree = treeDraw((uv-vec2(x, -y))*vec2(1., 1.+n*.2), vec3(1.), blur);

    //color.rg = uv;

    color = mix(color, tree, tree.a);
    color.a = max(ground, tree.a);
    return color;
}

float hash21(vec2 p){
    p = fract(p*vec2(234.45, 765.34));
    p += dot(p, p+547.123);
    return fract(p.x*p.y);
}

void main(){
    vec2 uv = (gl_FragCoord.xy-.5*u_resolution.xy)/u_resolution.y;
    
    //uv *= 1.5;
    vec2 M = (u_mouse.xy / u_resolution.xy) * 3.-1.;
    float t = u_time * .3;

    float blur = .005;

    float twinkle = dot(length(sin(uv+t)), length(cos(uv*vec2(22., 6.7)-t*3.)));
    twinkle = sin(twinkle*10.)*.5+.5;
    float stars = pow(hash21(uv), 100.)*twinkle;
    vec4 color = vec4(stars);

    float moon = S(.01, -.00, length(uv-vec2(.3, .15))-.1);
    color *= 1.-moon;   //if *= moon stars will inside the moon
    moon *= S(-.01, .07, length(uv-vec2(.35, .18))-.1);
    color += moon;

    uv.x += u_time*.005;

    vec4 layer;
    for(float i = 0.; i < 1.; i+=1./10.){
        float scale = mix(30., 1., i);
        blur = mix(.06, .005, i);
        layer = layerDraw(uv*scale+vec2(t+i*100., i)-M, blur);
        layer.rgb *= (1.-i)*vec3(0.8, 0.8, 1.);

        color = mix(color, layer, layer.a);
    }
    layer = layerDraw(uv+vec2(t, 2)-M, .05);
    color = mix(color, layer*.1, layer.a);
    
    //color += twinkle;
    float thickness = 1./u_resolution.y;
    // if(abs(uv.x)<thickness) color.g = 1.;
    // if(abs(uv.y)<thickness) color.r = 1.;
    

    gl_FragColor = color;
}