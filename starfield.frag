#ifdef GL_ES
precision mediump float;
#endif

uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
#define NUM_LAYERS 4.
mat2 rotate(float a){
    float s = sin(a), c=cos(a);
    return mat2(c, -s, s, c);
}

float starDraw(vec2 uv, float flare){
    float d = length(uv);
    float m = .05/d; //more like light

    float rays = max(0., 1.-abs(uv.x*uv.y*1000.));
    m += rays*flare;
    uv *= rotate(3.1415/4.);
    rays = max(0., 1.-abs(uv.x*uv.y*1000.));
    m += rays*.3*flare;

    m *= smoothstep(1., .2, d); //fade out
    return m;
}

float hash21(vec2 p){
    p = fract(p*vec2(123.34, 456.21));
    p += dot(p, p+45.32);
    return fract(p.x*p.y);
}

vec3 starLayer(vec2 uv){
    vec3 color = vec3(0.0);

    vec2 gv = fract(uv)-.5;
    vec2 id = floor(uv);

    for(int y=-1;y<=1;y++){
        for(int x=-1;x<=1;x++){
            vec2 offset = vec2(x, y);

            float n = hash21(id+offset); //random between 0 and 1
            float size = fract(n*345.32);

            float star = starDraw(gv-offset-vec2(n, fract(n*34.))+.5, smoothstep(.85, 1., size)*.6);
            vec3 col = sin(vec3(.2, .3, .9)*fract(n*2345.2)*123.2)*.5+.5;
            col = col*vec3(.9,.9,1.+size); //take out green
            
            //star *= sin(u_time*3.+n*6.2831)*5.+.5;
            star *= sin(u_time*3.+n*6.2831)*.5+1.;
            color += star*size*col;
        }
    }

    return color;
}

void main(){
    vec2 uv = (gl_FragCoord.xy-.5*u_resolution.xy) / u_resolution.y;
    vec2 M = (u_mouse.xy-u_resolution.xy*.5)/u_resolution.y;
    float t = u_time*.05;

    //uv += M*2.;
    uv *= rotate(t+.5);
    vec3 color = vec3(0);
    color += starLayer(uv);
    
    for(float i = 0.; i < 1.; i+=1./NUM_LAYERS){
        float depth = fract(i+t);
        float scale = mix(20., .5, depth);
        float fade = depth*smoothstep(1., .9, depth);

        color += starLayer(uv*scale+i*453.2/*-M*/)*fade;
    }
    
    //if(gv.x > .48 || gv.y > .48) color.r = 1.;

    //color.rg += id*.4;

    gl_FragColor = vec4(color, 1.0);
}