#ifdef GL_ES
precision mediump float;
#endif

uniform vec2 u_resolution;
uniform vec2 u_mouse;

float circleShape(vec2 position, float radius){
    return step(radius, length(position));
}

void main(){
    vec2 coord = gl_FragCoord.xy / u_resolution;
    vec3 color = vec3(0.0);

    vec2 translate = vec2(0., 1.0);
    coord += -u_mouse / u_resolution;

    color += vec3(circleShape(coord, 0.3));
    color.r += u_mouse.x / u_resolution.x;

    gl_FragColor = vec4(color, 1.0);
}