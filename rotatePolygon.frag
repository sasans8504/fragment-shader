#ifdef GL_ES
precision mediump float;
#endif

const float PI = 3.1415926535;
uniform vec2 u_resolution;
uniform float u_time;

float polygonShape(vec2 position, float radius, float sides){
    position = position * 2.0 - 1.0;
    float angle = atan(position.x, position.y);
    float slice = PI * 2.0 / sides;

    return step(radius, cos(floor(0.6 + angle / slice) * slice - angle) * length(position));
}

mat2 rotate(float angle){
    return mat2(cos(angle), -sin(angle), sin(angle), cos(angle));
}

void main(){
    vec2 coord = gl_FragCoord.xy / u_resolution;
    vec3 color = vec3(0.0824, 0.5922, 0.1922);

    coord -= vec2(0.5);
    coord = rotate(-u_time * 4.0) * coord;
    coord += vec2(0.5);

    color += vec3(polygonShape(coord, 0.3, 6.0));

    gl_FragColor = vec4(color, 1.0);
}